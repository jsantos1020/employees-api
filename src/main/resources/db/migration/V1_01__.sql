CREATE TABLE employees (
                           id uuid NOT NULL,
                           name text DEFAULT NULL,
                           email text DEFAULT NULL,
                           created_at timestamp NOT NULL,
                           PRIMARY KEY (id)
);

CREATE TABLE address_employees (
                                   id uuid DEFAULT NULL,
                                   type text DEFAULT NULL,
                                   address text DEFAULT NULL,
                                   employees_id uuid NOT NULL
);