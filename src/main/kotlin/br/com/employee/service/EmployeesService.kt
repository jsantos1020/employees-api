package br.com.employee.service

import br.com.employee.model.Employees
import br.com.employee.repository.EmployeesRepository
import jakarta.transaction.Transactional
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.UUID

@Service
data class EmployeesService(
    private val repository: EmployeesRepository
) {

    @Transactional
    fun save(employees: Employees): Employees {
        runCatching {
            return repository.save(employees)
        }.getOrElse {
            throw Exception("Não foi possível criar Employee: ${it.message}")
        }
    }

    fun findById(employeeId: UUID): Employees {
        return repository.findByIdOrNull(employeeId) ?: throw Exception("Employee não encontrado")
    }
}
