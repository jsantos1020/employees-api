package br.com.employee.service

import br.com.employee.model.AddressEmployees
import br.com.employee.repository.AddressEmployeesRepository
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service

@Service
class AddressEmployeesService(
    private val repository: AddressEmployeesRepository,
    private val employeesService: EmployeesService
) {

    @Transactional
    fun saveAll(addressEmployees: List<AddressEmployees>): List<AddressEmployees> {
        return repository.saveAll(addressEmployees)
    }
}
