package br.com.employee.dto

import br.com.employee.model.TypeAddress
import java.io.Serializable

data class ListAddressEmployeesRequest(
    val address: List<AddressEmployeesDto>
) : Serializable

data class AddressEmployeesDto(
    val type: TypeAddress,
    val address: String
) : Serializable

data class ListAddressEmployeesResponse(
    val employee: EmployeeRequestDto,
    val address: List<AddressEmployeesDto>
) : Serializable
