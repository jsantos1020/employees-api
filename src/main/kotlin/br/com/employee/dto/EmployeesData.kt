package br.com.employee.dto

import java.io.Serializable
import java.time.Instant
import java.util.UUID

data class EmployeeRequestDto(
    val name: String,
    val email: String
) : Serializable

data class EmployeeResponseDto(
    val id: UUID,
    val name: String,
    val email: String,
    val createdAt: Instant
) : Serializable
