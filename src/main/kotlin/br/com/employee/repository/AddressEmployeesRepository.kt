package br.com.employee.repository

import br.com.employee.model.AddressEmployees
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface AddressEmployeesRepository : JpaRepository<AddressEmployees, UUID>
