package br.com.employee.model

import br.com.employee.utils.NoArg
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.Instant
import java.util.UUID

@Entity
@Table(name = "employees")
@NoArg
data class Employees(
    @Id
    var id: UUID = UUID.randomUUID(),
    var name: String,
    var email: String,
    @Column(name = "created_at")
    val createdAt: Instant = Instant.now()
)
