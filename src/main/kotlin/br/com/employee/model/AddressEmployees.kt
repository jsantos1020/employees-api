package br.com.employee.model

import br.com.employee.utils.NoArg
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import java.util.UUID

@Entity
@Table(name = "address_employees")
@NoArg
data class AddressEmployees(
    @Id
    var id: UUID = UUID.randomUUID(),
    @Enumerated(EnumType.STRING)
    var type: TypeAddress,
    var address: String,
    @ManyToOne
    @JoinColumn(name = "employees_id")
    var employee: Employees
)

enum class TypeAddress {
    COMERCIAL, PESSOAL
}
