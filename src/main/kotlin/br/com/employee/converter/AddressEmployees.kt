package br.com.employee.converter

import br.com.employee.dto.AddressEmployeesDto
import br.com.employee.dto.EmployeeRequestDto
import br.com.employee.dto.ListAddressEmployeesRequest
import br.com.employee.dto.ListAddressEmployeesResponse
import br.com.employee.model.AddressEmployees
import br.com.employee.service.EmployeesService
import java.util.UUID

fun ListAddressEmployeesRequest.toModel(employeesService: EmployeesService, employeeId: UUID): List<AddressEmployees> {
    val employee = employeesService.findById(employeeId = employeeId)
    return this.address.map {
        AddressEmployees(
            type = it.type,
            address = it.address,
            employee = employee
        )
    }
}

fun List<AddressEmployees>.toResponseDto(): ListAddressEmployeesResponse {
    val employee = EmployeeRequestDto(name = this.first().employee.name, email = this.first().employee.email)
    return ListAddressEmployeesResponse(
        employee = employee,
        address = this.map {
            AddressEmployeesDto(type = it.type, address = it.address)
        }
    )
}