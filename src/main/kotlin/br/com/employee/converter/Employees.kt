package br.com.employee.converter

import br.com.employee.dto.EmployeeRequestDto
import br.com.employee.dto.EmployeeResponseDto
import br.com.employee.model.AddressEmployees
import br.com.employee.model.Employees

fun EmployeeRequestDto.toModel() = Employees(
    name = this.name,
    email = this.email
)

fun Employees.toResponseDto() = EmployeeResponseDto(
    id = this.id,
    name = this.name,
    email = this.email,
    createdAt = this.createdAt
)
