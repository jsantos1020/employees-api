package br.com.employee.controller

import br.com.employee.converter.toModel
import br.com.employee.converter.toResponseDto
import br.com.employee.dto.ListAddressEmployeesRequest
import br.com.employee.dto.ListAddressEmployeesResponse
import br.com.employee.service.AddressEmployeesService
import br.com.employee.service.EmployeesService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.UUID

@Controller
@RequestMapping("/v1/address")
class AddressEmployeesController(
    private val service: AddressEmployeesService,
    private val employeesService: EmployeesService
) {

    @PostMapping("/{employeeId}/employees")
    fun save(
        @RequestBody request: ListAddressEmployeesRequest,
        @PathVariable employeeId: UUID
    ): ResponseEntity<ListAddressEmployeesResponse> {
        val address = service.saveAll(request.toModel(employeesService, employeeId))
        return ResponseEntity.ok(address.toResponseDto())
    }
}