package br.com.employee.controller

import br.com.employee.converter.toResponseDto
import br.com.employee.dto.EmployeeResponseDto
import br.com.employee.model.Employees
import br.com.employee.service.EmployeesService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus

@Controller
@RequestMapping("/v1/employees")
class EmployeesController(private val service: EmployeesService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody request: Employees): ResponseEntity<EmployeeResponseDto> {
        val employeeCreated = service.save(request)
        return ResponseEntity.ok(employeeCreated.toResponseDto())
    }
}
